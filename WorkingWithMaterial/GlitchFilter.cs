﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class GlitchFilter : MonoBehaviour
{
    public enum EType { None, CyberpunkOne, Glitch }


    public int debugEffect;
    [SerializeField] EffectData[] effects;

    Material _activeMaterial;



    public EType Effect
    {
        set
        {
            _activeMaterial = FindMaterialForEffect(value);
            this.enabled = _activeMaterial != null;
        }
    }

    private void OnValidate()
    {
        this.Effect = (EType)debugEffect;
    }

    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        Debug.Assert(_activeMaterial != null);
        Graphics.Blit(source, destination, _activeMaterial);
    }


    Material FindMaterialForEffect(EType type)
    {
        if (type == EType.None)
            return null;

        int i = Array.FindIndex(effects, x => x.type == type);
        Debug.Assert(i >= 0, type);

        return i >= 0 ? effects[i].material : null;
    }




    [Serializable] private struct EffectData
    {
        public EType type;
        public Material material;
    }

}