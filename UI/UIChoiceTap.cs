﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Assertions;
using UnityEngine.EventSystems;
using TMPro;




namespace View.DialogSystem
{
    public class UIChoiceTap : UIChoiceGestureBase
    {

        [SerializeField] Button _tapButton;
        [SerializeField] Animator _anim;
        [SerializeField] TextMeshProUGUI _text;

        private int _choiceIndex;

        public bool Interactable {
            set => _tapButton.interactable = value;
        }

        public bool FailIconVisible {
            set { _tapButton.transform.GetChild(0).gameObject.SetActive(value); }
        }



        //.....................................................................................UNITY

        void Awake()
        {
            _tapButton.onClick.AddListener( () => StartCoroutine(TapSuccesCR(_choiceIndex)) );
        }

        void OnEnable()
        {
            _text.text = this.GetLocalizedText(ETextType.Tap);
            FailIconVisible = false;
        }

        //..................................................................................OVERRIDE

        public override void Setup(object data)
        {
            _choiceIndex = (int)data;
            Interactable = true;
        }

        public override void SetFailed(int result)
        {
            StartCoroutine(TapTimerFailedCR(result));
        }

        //................................................................................COROUTINES

        private IEnumerator TapTimerFailedCR(int result)
        {
            Interactable = false;
            _anim.SetTrigger(HashOnFail);
            _text.text = this.GetLocalizedText(ETextType.Fail);
            FailIconVisible = true;
            yield return new WaitForAnimator(_anim);
            this.SetResult(result);
        }

        private IEnumerator TapSuccesCR(int result)
        {
            TriggerSucces();
            Interactable = false;
            _anim.SetTrigger(HashOnSuccess);
            _text.text = this.GetLocalizedText(ETextType.Success);
            yield return new WaitForAnimator(_anim);
            this.SetResult(result);
        }
    }
}
