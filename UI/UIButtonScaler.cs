using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;




namespace View
{
    public class UIButtonScaler : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
    {
        [SerializeField] bool  useCustomScale;
        [SerializeField] float customScale = 1.5f;

        float curr;
        float target;
        Vector3 normalScale;
        RectTransform rxf;



        void Awake()
        {
            rxf = (RectTransform)transform;
            normalScale = rxf.localScale;
        }


        void Update()
        {
            if (curr != target)
            {
                float step = UIDataProvider.buttonScaler.speed * Time.deltaTime;
                curr = Mathf.MoveTowards(curr, target, step);
                float t = UIDataProvider.buttonScaler.curve.Evaluate(curr);
                float scale = useCustomScale ? customScale : UIDataProvider.buttonScaler.scale;
                rxf.localScale = normalScale * Mathf.Lerp(1f, scale, t);
            }
        }


        void OnValidate()
        {
            if (((transform as RectTransform).pivot - Vector2.one * 0.5f).magnitude > 0.01f)
                Debug.LogError("[UIButtonScaler] requires pivot to be in the middle of the rect (ping)", this);
        }


        public void OnPointerDown(PointerEventData eventData) => target = 1f;
        public void OnPointerUp(PointerEventData eventData)   => target = 0f;
    }
}