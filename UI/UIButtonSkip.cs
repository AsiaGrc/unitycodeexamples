using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

using ESkipType     = ApplicationSettings.SkipButton.EType;





namespace View
{
    [RequireComponent(typeof(Image))]
    public class UIButtonSkip : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
    {

        [SerializeField] float holdTime = 1f;
        [SerializeField] Image fillImage;
        [SerializeField] AnimationCurve fillCurve;
        [SerializeField] Color pressedColor;

        float downTime;
        ESkipType skipType;
        Color normalColor;


        public event Action OnClick;


        void Awake()
        {
            normalColor = fillImage.color;
        }


        void OnEnable()
        {
            fillImage.fillAmount = 0f;
            skipType = ApplicationSettings.SkipButton.Type;
        }


        void Update()
        {
            if (downTime <= 0f) return;
            if (skipType == ESkipType.Delayed)
            {
                float normalizedTime = (Time.unscaledTime - downTime) / holdTime;
                fillImage.fillAmount = fillCurve.Evaluate(normalizedTime);
                if (normalizedTime >= 1f)
                {
                    downTime = 0f;
                    OnClick.Invoke();
                }
            }
        }


        public void OnPointerDown(PointerEventData eventData)
        {
            downTime = Time.unscaledTime;
            fillImage.color = pressedColor;
            if (skipType == ESkipType.Touch)
            {
                OnClick.Invoke();
            }
        }


        public void OnPointerUp(PointerEventData eventData)
        {
            downTime = 0f;
            fillImage.fillAmount = 0f;
            fillImage.color = normalColor;
            if (skipType == ESkipType.Click)
            {
                OnClick.Invoke();
            }
        }

    }
}