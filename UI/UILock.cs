using System;
using UnityEngine;
using UnityEngine.EventSystems;




namespace View
{
    /// <summary>
    /// Disable unity ui event system for a period of time
    /// </summary>
    public class UILock : IDisposable
    {
        static EventSystem eventSystem;
        bool isDisposed;

        //......................................................................................CTOR

        public UILock(bool autolock = true)
        {
            if (eventSystem == null)
                eventSystem = EventSystem.current;

            if (autolock)
               SetLockState(true);
        }

        //....................................................................................PUBLIC

        public void Lock()   => SetLockState(true);
        public void Unlock() => SetLockState(false);

        //...............................................................................IDISPOSABLE

        public void Dispose()
        {
            if (!isDisposed)
                SetLockState(false);
            isDisposed = true;
        }

        //...................................................................................PRIVATE

        private void SetLockState(bool isLocked)
        {
            if (eventSystem != null)
                eventSystem.enabled = !isLocked;
            else
                Debug.LogError("Current EventSystem is null");
        }

    }
}