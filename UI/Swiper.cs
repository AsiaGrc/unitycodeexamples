using System;
using UnityEngine;





namespace View
{
	public class Swiper : MonoBehaviour
	{
		[SerializeField] float _minSwipeDist = 60;
		private Vector2 _startPos;

		public event Action<Direction> OnSwipe;
		public enum Direction { Left, Right, Up, Down }

		void Update()
		{
			if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
			{
				_startPos = Input.GetTouch(0).position;

				Debug.Log("SwipeStart");
			}

			if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Ended)
			{
				Vector2 swipe = Input.GetTouch(0).position - _startPos;

				if (swipe.magnitude >= _minSwipeDist)
				{
					if (Mathf.Abs(swipe.x) > Mathf.Abs(swipe.y))
						OnSwipe.Invoke(swipe.x > 0 ? Direction.Right : Direction.Left);
					else
						OnSwipe.Invoke(swipe.y > 0 ? Direction.Down : Direction.Up);
				}
			}
		}
	}
}
