﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Assertions;
using UnityEngine.EventSystems;
using TMPro;




namespace View.DialogSystem
{
    public class UIChoiceSliderBidirectional : UIChoiceGestureBase//, IDragHandler, IBeginDragHandler, IEndDragHandler
    {
        [SerializeField] Animator _anim;
        [SerializeField] CanvasGroup _dragAreaCanvasGroup;
        [SerializeField] Slider _sliderLeft;
        [SerializeField] Slider _sliderRight;
        [SerializeField] GameObject _gostObject;
        [SerializeField] TextMeshProUGUI _text;
        [SerializeField] float _submitFailedGoBackTime = 0.4f;

        private Sprite _restoreArrowSprite;
        private Vector3 _restoreFillScale;

        private int _leftChoiceIndex, _rightChoiceIndex;


        //..................................................................................PROPERTY

        private bool IsDragAreaIconVisible {
            set => _dragAreaCanvasGroup.alpha = value ? 1f : 0f;
        }

        private bool IsGostVisible {
            set {
                _gostObject.SetActive(value);
                _anim.SetBool(HashIsGostOn, value);
            }
        }

        private bool Interactable {
            get => _dragAreaCanvasGroup.interactable;
            set => _dragAreaCanvasGroup.interactable = value;
        }

        //.....................................................................................UNITY

        void Awake()
        {
            _sliderRight.onValueChanged.AddListener( f => OnSliderChanged(true, f) );
            _sliderLeft.onValueChanged.AddListener( f => OnSliderChanged(false, f) );
            _restoreArrowSprite = _sliderRight.handleRect.GetChild(0).GetComponent<Image>().sprite;
            _restoreFillScale = Vector3.one;
        }

        void OnEnable()
        {
            _sliderLeft.SetValueWithoutNotify(0f);
            _sliderRight.SetValueWithoutNotify(0f);
            Interactable = true;
            _text.text = this.GetLocalizedText(ETextType.SwipeLeftOrRight);
        }

        void OnDisable()
        {
            _sliderRight.handleRect.GetChild(0).GetComponent<Image>().sprite = _restoreArrowSprite;
            _sliderLeft .handleRect.GetChild(0).GetComponent<Image>().sprite = _restoreArrowSprite;

            _sliderLeft .fillRect.localScale = _restoreFillScale;
            _sliderRight.fillRect.localScale = _restoreFillScale;
        }


        //...................................................................................U-CALLS

        public void OnAreaDrag_UCall(BaseEventData ev)
        {
            var pData = (PointerEventData)ev;
            bool isRight = pData.delta.x > 0f;

            IsDragAreaIconVisible = false;
            IsGostVisible = false;
            ShowSlider(isRight ? 2 : 1);

            var s = isRight ? _sliderRight : _sliderLeft;
            s.SendMessage("OnDrag", pData);

        }


        public void OnPointerUp_UCall(BaseEventData ev)
        {
            if (Interactable)
                StartCoroutine(SliderDragFailCR());
        }

        //...................................................................................PRIVATE

        private void OnSliderChanged(bool isRight, float f)
        {
            if (Mathf.Approximately(f, 1f))
                StartCoroutine(SliderDragSuccesCR(isRight ? _rightChoiceIndex : _leftChoiceIndex));
        }

        private void ShowSlider(int mode)
        {
            switch (mode)
            {
                case 0:
                    _sliderLeft.gameObject.SetActive(false);
                    _sliderRight.gameObject.SetActive(false);
                    break;
                case 1:
                    _sliderLeft.gameObject.SetActive(true);
                    _sliderRight.gameObject.SetActive(false);
                    break;
                case 2:
                    _sliderLeft.gameObject.SetActive(false);
                    _sliderRight.gameObject.SetActive(true);
                    break;
                default: throw new System.NotImplementedException($"wha: {mode}");
            }
        }

        //.................................................................................OVERRIDES

        public override void Setup(object data)
        {
            (_leftChoiceIndex,_rightChoiceIndex) = ((int,int))data;
        }

        public override void SetFailed(int result)
        {
            // no base call
            StartCoroutine(SliderTimerFailedCR(result));
        }

        //................................................................................COROUTINES

        private IEnumerator SliderDragFailCR()
        {
            var slider = _sliderRight.gameObject.activeSelf ? _sliderRight : _sliderLeft;
            Assert.IsTrue(slider.gameObject.activeSelf);
            float stime = Time.time, f = 0f, val = slider.value;
            Interactable = false;

            while (f < 1f)
            {
                f = (Time.time - stime) / _submitFailedGoBackTime;
                f = f < 1f ? f : 1f;
                slider.SetValueWithoutNotify(Mathf.Lerp(val, 0f, f));
                yield return null;
            }

            IsGostVisible = true;
            IsDragAreaIconVisible = true;
            _dragAreaCanvasGroup.interactable = true;
        }

        private IEnumerator SliderDragSuccesCR(int result)
        {
            TriggerSucces();
            Interactable = false;
            _anim.SetTrigger(HashOnSuccess);
            _text.text = this.GetLocalizedText(ETextType.Success);
            yield return new WaitForAnimator(_anim);
            this.SetResult(result);
        }

        private IEnumerator SliderTimerFailedCR(int result)
        {
            IsGostVisible = false;
            Interactable = false;
            _anim.SetTrigger(HashOnFail);
            _text.text = this.GetLocalizedText(ETextType.Fail);
            yield return new WaitForAnimator(_anim);
            this.SetResult(result);
        }

    }
}