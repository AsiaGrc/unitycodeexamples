﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ChoicePanel : UIWidget
{
    Button[] choiceButtons;

    public override void Setup(IInfoUI infoUI)
    {
        var info = (ChoiceInfo)infoUI;

        choiceButtons = GetComponentsInChildren<Button>();
        foreach(var button in choiceButtons)
        {
            button.gameObject.SetActive(false);
            button.onClick.RemoveAllListeners();
        }

        for(int i = 0; i < info.choicesText.Length; i++)
        {
            var choiceText = choiceButtons[i].GetComponentInChildren<TextMeshProUGUI>();
            choiceText.text = info.choicesText[i];
            choiceButtons[i].gameObject.SetActive(true);

            int choiceIndex = info.indices[i];
            choiceButtons[i].onClick.AddListener(() => onDone.Invoke(choiceIndex));
        }
    }

    public struct ChoiceInfo :IInfoUI
    {
        public string[] choicesText;
        public int[] indices;
    }
}
