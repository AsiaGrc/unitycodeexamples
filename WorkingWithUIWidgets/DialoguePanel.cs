﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;



public class DialoguePanel : UIWidget
{
    [SerializeField] Transform dialogueStack;
    [SerializeField] DialogueLine dialoguePrototype;
    [SerializeField] Button button;

    public override void Setup(IInfoUI infoUI)
    {
        var info = (DialogueInfo)infoUI;
        var dp = Instantiate(dialoguePrototype, Vector3.zero, Quaternion.identity, dialogueStack);
        dp.transform.SetSiblingIndex(1);
        dp.transform.localPosition = Vector3.zero;
        dp.Setup(infoUI);
        button.onClick.RemoveAllListeners();
        button.onClick.AddListener(() => onDone.Invoke());
    }

    public struct DialogueInfo : IInfoUI
    {
        public string name;
        public string imageId;
        public string phrase;
    }
}
