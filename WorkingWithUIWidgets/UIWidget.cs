﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IInfoUI { }

public abstract class UIWidget : MonoBehaviour
{
    public static T FindWidget<T>() where T : UIWidget
    {
        GameObject canvas = GameObject.FindGameObjectWithTag(Const.Tags.chatTag);
        T child = canvas.GetComponentInChildren<T>(true);
        Debug.Assert(child != null, $"Object of type {typeof(T)} isn't found in scene");
        return child;
    }

    public abstract void Setup(IInfoUI info);

    public void Show() 
    {
        this.gameObject.SetActive(true);
    }

    public void Hide()
    {
        this.gameObject.SetActive(false);
    }
}