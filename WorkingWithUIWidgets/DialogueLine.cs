﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DialogueLine : UIWidget
{
    [SerializeField] TextMeshProUGUI tmp;

    public override void Setup(IInfoUI infoUI)
    {
        var info = (DialoguePanel.DialogueInfo)infoUI;
        tmp.text = info.phrase;
    }
}
