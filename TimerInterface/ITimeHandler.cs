﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System.Text.RegularExpressions;

public interface ITimeHandler
{
    DateTime CurrentTime();
}


public class InternetTime : ITimeHandler
{
    const string API_Url = "https://worldtimeapi.org/api/ip";

    DateTime _currentDateTime = DateTime.Now;

    IEnumerator GetRealDateTimeFromAPI()
    {
        UnityWebRequest webRequest = UnityWebRequest.Get(API_Url);

        yield return webRequest.SendWebRequest();

        if (webRequest.isNetworkError)
        {
            Debug.Log("Error:" + webRequest.error);
        }
        else
        {
            TimeData timeData = JsonUtility.FromJson<TimeData>(webRequest.downloadHandler.text);
            _currentDateTime = ParseDateTime(timeData.dateTime);
        }
    }

    DateTime ParseDateTime(string value)
    {
        string dateTime = Regex.Match(value, @"^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}").Value;

        return DateTime.Parse(dateTime);
    }

    public DateTime CurrentTime()
    {
        //maybe will be needed changes
        GetRealDateTimeFromAPI();
        return _currentDateTime;
    }
}

public class LocalTime : ITimeHandler
{
    DateTime _currentDateTime;

    void GetLocalDateTime()
    {
        _currentDateTime = DateTime.UtcNow;
    }

    public DateTime CurrentTime()
    {
        GetLocalDateTime();
        return _currentDateTime;
    }
}

public struct TimeData
{
    public string dateTime;
}

