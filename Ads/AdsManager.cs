using System;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Advertisements;
using UnityEngine.Assertions;

namespace Managers
{
    public class AdsManager : Manager, IUnityAdsLoadListener, IUnityAdsListener, IUnityAdsInitializationListener
    {
        public enum EAdType { Rewarded, Interstitial }


        private TaskCompletionSource<bool>          _tcsInit;
        private TaskCompletionSource<string>        _tcsLoadAd;
        private TaskCompletionSource<ShowResult>    _tcsShowAd;
        private int                                 _randomAdDice;



        //.....................................................................................C-TOR


        public AdsManager()
        {
            _tcsInit = new TaskCompletionSource<bool>();
            var cfg = ApplicationConfig.Instance.ads;
            Advertisement.AddListener((IUnityAdsListener)this);
            Advertisement.Initialize(cfg.PlatformGameId, cfg.isTestMode, cfg.isPerPlacementLifecycle, (IUnityAdsInitializationListener)this);
        }

        //....................................................................................PUBLIC

        public async Task<bool> ShowAd(bool IsAdRewardable)
        {
            if (!Advertisement.isInitialized || !ApplicationConfig.Instance.ads.isEnabled)
                return false;
            if (!IsAdRewardable && ApplicationConfig.Instance.ads.disableNonRewardable)
                return false;

            var adType = IsAdRewardable ? EAdType.Rewarded : EAdType.Interstitial;
            try
            {
                if(!Advertisement.IsReady(GetAdUnitId(adType)))
                    await LoadAdAsync(adType, CancellationToken.None);
                var result = await ShowAdInternalAsync(adType, CancellationToken.None);
                bool finished = result == ShowResult.Finished;
                if (IsAdRewardable && finished)
                    Manager.Profile.User.TimeRewardedAdShown = DateTime.Now;

                return finished;
            }
            catch (Exception e)
            {
                Debug.LogError($"Ads Exception catch: {e}");
                // handle ui elements state on error
                return false;
            }
        }


        public Task TryShowRandomAdAsync()
        {
            if (_randomAdDice <= 0)
                return Task.CompletedTask;

            int rand = UnityEngine.Random.Range(0, _randomAdDice);
            _randomAdDice--;

            if (rand == 0)
            {
                _randomAdDice = -1;
                return this.ShowAd(false);
            }
            else
                return Task.CompletedTask;
        }


        public void SetRandomAdDice(int dice)
        {
            _randomAdDice = dice;
        }


        public async Task<bool> GetRewardedAdAvailability()
        {
            try
            {
                if (!Advertisement.isInitialized)
                {
                    var inited = await _tcsInit.Task;
                    if (!inited) return false;
                }
                await LoadAdAsync(EAdType.Rewarded, CancellationToken.None);
                return true;
            }
            catch (Exception e)
            {
                Debug.LogError(e);
                return false;
            }
        }


        /// <summary>
        /// returns the time in seconds since last rewarded ad was shown </summary>
        public int GetTimeRewardedAdsShown()
        {
            var secondsPassed = (DateTime.Now - Manager.Profile.User.TimeRewardedAdShown).TotalSeconds;
            if (secondsPassed < int.MaxValue)
                return (int)secondsPassed;
            else
                return int.MaxValue;
        }

        //..........................................................INTERFACE: IUnityAdsLoadListener

        void IUnityAdsLoadListener.OnUnityAdsAdLoaded(string placementId)
        {
            Debug.Assert(_tcsLoadAd != null, $"did call {nameof(LoadAdAsync)}() ?");
            placementId.ThrowIfEmpty();

            var tcs = _tcsLoadAd;
            _tcsLoadAd = null;
            tcs.SetResult(placementId);
            CLog.Info("Ads loaded");
        }


        void IUnityAdsLoadListener.OnUnityAdsFailedToLoad(string placementId, UnityAdsLoadError error, string message)
        {
            var msg = $"Error loading Ad. '{placementId}' | {error} | {message}";
            var tcs = _tcsLoadAd;
            _tcsLoadAd = null;
            tcs?.SetException(new OperationCanceledException(msg));
        }


        //................................................INTERFACE: IUnityAdsInitializationListener


        void IUnityAdsInitializationListener.OnInitializationComplete()
        {
            var tcs = _tcsInit;
            _tcsInit = null;
            CLog.Info("[AdsManager] inited successful", "green");
            tcs?.SetResult(true);
        }


        void IUnityAdsInitializationListener.OnInitializationFailed(UnityAdsInitializationError error, string message)
        {
            var tcs = _tcsInit;
            _tcsInit = null;
            Debug.LogError($"Unity Ads Initialization Failed: {error.ToString()} - {message}");
            tcs?.SetResult(false);
        }

        //..............................................................INTERFACE: IUnityAdsListener


        void IUnityAdsListener.OnUnityAdsReady(string placementId)
        {
            CLog.Info($"ad ready: '{placementId}'");
        }


        void IUnityAdsListener.OnUnityAdsDidError(string message)
        {
            var cts = _tcsShowAd;
            _tcsShowAd = null;
            cts?.SetException(new OperationCanceledException($"unity can't show add '{message}'"));
        }


        void IUnityAdsListener.OnUnityAdsDidStart(string placementId)
        {
            CLog.Info($"start show add: '{placementId}'");
        }


        void IUnityAdsListener.OnUnityAdsDidFinish(string placementId, ShowResult showResult)
        {
            switch (showResult)
            {
                case ShowResult.Finished:
                    if (IsAdRewardable(placementId))
                        AdWatchedGiveReward();
                    break;
                    // default: do nothing
            }

            Analytics.AdWatched(placementId);

            CLog.Info($"ad finished '{placementId}' wit result: {showResult}");
            var tcs = _tcsShowAd;
            _tcsShowAd = null;
            tcs.SetResult(showResult);
        }


        //...................................................................................PRIVATE

        private Task<string> LoadAdAsync(EAdType adType, CancellationToken token)
        {
            Assert.IsNull(_tcsLoadAd);
            token.ThrowIfCancellationRequested();

            string adUnitId = GetAdUnitId(adType);
            _tcsLoadAd = new TaskCompletionSource<string>();
            Advertisement.Load(adUnitId, this);
            CLog.Info("Loading Ad: " + adUnitId);
            return _tcsLoadAd.Task;
        }


        private Task<ShowResult> ShowAdInternalAsync(EAdType adType, CancellationToken token)
        {
            Assert.IsNull(_tcsShowAd);
            token.ThrowIfCancellationRequested();

            _tcsShowAd = new TaskCompletionSource<ShowResult>();
            string adUnitId = GetAdUnitId(adType);
            Advertisement.Show(adUnitId);
            return _tcsShowAd.Task;
        }


        private string GetAdUnitId(EAdType adType)
        {
            switch (adType)
            {
                case EAdType.Interstitial: return ApplicationConfig.Instance.ads.PlatformIdInterstitial;
                case EAdType.Rewarded:     return ApplicationConfig.Instance.ads.PlatformIdRewarded;
                default: throw new System.NotImplementedException(adType.ToString());
            }
        }


        private bool IsAdRewardable(string placementId)
        {
            return placementId == ApplicationConfig.Instance.ads.PlatformIdRewarded;
        }


        private async void AdWatchedGiveReward()
        {
            await Manager.Rewards.GiveReward(ERewardType.AdWatched);
        }

    }
}