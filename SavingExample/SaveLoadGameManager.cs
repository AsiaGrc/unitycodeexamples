using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;



public sealed class SaveLoadGameManager : IManager
{
    static SaveLoadGameManager _instance;

    public static SaveLoadGameManager Instance => _instance = _instance ?? new SaveLoadGameManager();


    private DefaultsProvider __dp;
    private DefaultsProvider Provider
    {
        get
        {
            if(__dp == null)
            {
                __dp = Resources.Load<DefaultsProvider>(Const.Paths.defaultsProvider);
                Debug.Assert(__dp != null);
            }
            return __dp;
        }
    }


    private SaveLoadGameManager() { }


    public void Save<T>(T data) where T : ISaveData
    {
        string path = GetPath<T>();
        string json = JsonUtility.ToJson(data);
        File.WriteAllText(path, json);
    }

    public T Load<T>() where T : ISaveData
    {
        string path =  GetPath<T>();

        if(File.Exists(path) == false)
            CreateDefaultSaveFile<T>();

        string json = File.ReadAllText(path);
        T result = JsonUtility.FromJson<T>(json);
        return result;
    }

    public void NullSaveFile<T>() where T : ISaveData
    {
        string path = GetPath<T>();
        CreateDefaultSaveFile<T>();
    }

    void CreateDefaultSaveFile<T>() where T : ISaveData
    {
        T data = Provider.Get<T>();
        Save(data);
    }

    static string GetPath<T>() where T : ISaveData
    {
        var t = typeof(T);
        switch(t)
        {
            case Type type when type == typeof(ChaptersState):
                return Const.Paths.chapters;
            case Type type when type == typeof(ChatData):
                return Const.Paths.savedChat;
            case Type type when type == typeof(ProfileData):
                return Const.Paths.profile;
            default:
                throw new NotImplementedException($"For type {t}");
        }
    }


    void NullAllSavingStates()
    {
        CreateDefaultSaveFile<ChaptersState>();
        CreateDefaultSaveFile<ChatData>();
        CreateDefaultSaveFile<ProfileData>();
    }
}

