﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObject/DefaultFileSaveState", fileName = "SaveData")]
public class DefaultsProvider : ScriptableObject
{
    [SerializeField] ChaptersState chapterData;
    [SerializeField] ArtData artData;
    [SerializeField] TrophiesData trophiesData;
    [SerializeField] CrewData crewData;
    [SerializeField] CaptainData playerData;
    [SerializeField] KeyAcquisitionData keyData; 
    [SerializeField] TimerData timerData;



    public T Get<T>() where T : ISaveData
    {
        var t = typeof(T);
        switch(t)
        {
            case Type type when type == typeof(ChaptersState):
                return (T)(object) this.chapterData;
            case Type type when type == typeof(ArtData):
                return (T)(object) this.artData;
            case Type type when type == typeof(TrophiesData):
                return (T)(object) this.trophiesData;
            case Type type when type == typeof(CrewData):
                return (T)(object) this.crewData;
            case Type type when type == typeof(CaptainData):
                return (T)(object) this.playerData;
            case Type type when type == typeof(KeyAcquisitionData):
                return (T)(object) this.keyData;
            case Type type when type == typeof(TimerData):
                return (T)(object) this.timerData;
            default:
                throw new NotImplementedException($"For type {t}");
        }
    }
}
