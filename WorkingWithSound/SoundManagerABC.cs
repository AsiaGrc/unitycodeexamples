using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public abstract class SoundManagerABC : MonoBehaviour, IManager
{
    private SoundProvider __ac;
    public SoundProvider Provider
    {
        get
        {
            if(__ac == null)
            {
                __ac = Resources.Load<SoundProvider>(Const.Paths.musicData);
                Debug.Assert(__ac != null);
            }
            return __ac;
        }
    }

    public virtual void PlayClip(string clipId, float beginningTime, float fadeTime, bool loop, float delayTime)
    {
        Debug.Assert(!string.IsNullOrEmpty(clipId));
        AudioClip clip = Provider.GetClip(clipId, this.GetType());
        AudioSource source = GetAudioSource();
        source.clip = clip;
        source.loop = loop;
        source.time = beginningTime;

        if(delayTime > 0f)
            source.PlayDelayed(delayTime);
        else
            source.Play();

        if(fadeTime > 0f)
            StartCoroutine(VolumeFadeCoroutine(source, fadeTime, 1f, delayTime));
    }

    public virtual void PlayClip(string clipId)
    {
        PlayClip(clipId, 0f, 0f, true, 0f);
    }

    public virtual void PlayClip(string clipId, float beginningTime)
    {
        PlayClip(clipId, beginningTime, 0f, true, 0f);
    }

    public virtual void PlayClip(string clipId, float beginningTime, float fadeTime)
    {
        PlayClip(clipId, beginningTime, fadeTime, true, 0f);
    }

    public virtual void PlayClip(string clipId, float beginningTime, float fadeTime, bool loop)
    {
        PlayClip(clipId, beginningTime, fadeTime, loop, 0f);
    }


    public abstract SaveSoundData[] GetPlayingSounds();

    protected abstract void StopAllSounds();


    protected abstract AudioSource GetAudioSource();



    IEnumerator VolumeFadeCoroutine(AudioSource source, float fadeTime, float targetVolume, float delayTime)
    {
        yield return new WaitForSeconds(delayTime);

        float speed = 1f/fadeTime;
        source.volume = 1f - targetVolume; // invert

        while(source.volume != targetVolume)
        {
            source.volume = Mathf.MoveTowards(source.volume, targetVolume, speed * Time.deltaTime);
            yield return null;
        }
    }

    // AnimationCurve ac;
    // IEnumerator VV(float fxTime, bool invert)
    // {
    //     float stime = Time.time;
    //     float f = 0f;

    //     while (f < 1f)
    //     {
    //         f = (Time.time - stime) / fxTime;
    //         f = f < 1f ? f : 1f;
    //         //f = invert ? 1f - f : f;
    //         //Mathf.Lerp(-3, 5, f);
    //         yield return null;
    //     }
    // }
}



public abstract class SingleSourceSoundManager : SoundManagerABC
{
    AudioSource _source;

    protected AudioSource Source
    {
        get
        {
            if(_source == null)
            {
                _source = GetComponent<AudioSource>();
                Debug.Assert(_source != null);
            }
            return _source;
        }
    }

    protected override AudioSource GetAudioSource() => Source;
    protected override void StopAllSounds() => Source.Stop();

    public override SaveSoundData[] GetPlayingSounds()
    {
        if(Source.isPlaying == false)
            return new SaveSoundData[0];

        return new SaveSoundData[] {
            new SaveSoundData {
                clip = Source.clip.name,
                time = Source.time
            }
        };
    }
}



public abstract class MultipleSourceSoundManager : SoundManagerABC
{
    List<AudioSource> _sources;

    protected List<AudioSource> Sources
    {
        get
        {
            if(_sources == null)
            {
                _sources = new List<AudioSource>(GetComponents<AudioSource>());
                Debug.Assert(_sources.Count > 0);
            }
            return _sources;
        }
    }

    protected override AudioSource GetAudioSource()
    {
        var source = Sources.Find(x => x.isPlaying == false);
        return source ?? CreateAudioSource();
    }

    protected override void StopAllSounds()
    {
        for(int i = 0; i < Sources.Count; i++)
            Sources[i].Stop();
    }

    public override SaveSoundData[] GetPlayingSounds()
    {
        var query = from s in Sources
                    where s.isPlaying
                    select new SaveSoundData {clip = s.clip.name, time = s.time};
        return query.ToArray();
    }


    AudioSource CreateAudioSource()
    {
        var source = gameObject.AddComponent<AudioSource>();
        source.playOnAwake = false;
        source.loop = true;
        _sources.Add(source);
        return source;
    }
}