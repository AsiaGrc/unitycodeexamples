using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "ScriptableObject/SoundManager", fileName = "Music")]
public class SoundProvider : ScriptableObject
{
    [SerializeField] public List<AudioClip> ambient;
    [SerializeField] public List<AudioClip> music;
    [SerializeField] List<AudioClip> sfx;



    void OnEnable()
    {
        ValidateLoadType(ambient, AudioClipLoadType.Streaming);
        ValidateLoadType(music, AudioClipLoadType.Streaming);
        ValidateLoadType(sfx, AudioClipLoadType.DecompressOnLoad);
    }


    public AudioClip GetClip(string id, Type managerType)
    {
        Debug.Assert(!string.IsNullOrEmpty(id));
        var list = GetClipList(managerType);
        var clip = list.Find(c => c.name == id);
        Debug.Assert(clip != null, $"No clip with id {id}", this);
        return clip;
    }


    private List<AudioClip> GetClipList(Type type)
    {
        Debug.Assert(type != null);
        switch(type)
        {
            case Type t when t == typeof(AmbientManager):
                return ambient;
            case Type t when t == typeof(MusicManager):
                return music;
            case Type t when t == typeof(SFXManager):
                return sfx;
            default:
                throw new NotImplementedException($"For type {type}");
        }
    }


    private void ValidateLoadType(List<AudioClip> collection, AudioClipLoadType loadType)
    {
        for(int i = 0; i < collection.Count; i++)
            if(collection[i].loadType != loadType)
                Debug.LogError($"AudioClip {collection[i].name} load type {collection[i].loadType} != {loadType}", collection[i]);
    }
}
