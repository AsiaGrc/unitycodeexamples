﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Commands
{
    public abstract class Command 
    {
        public abstract bool isPreserved {get; protected set;}
        public virtual bool IsDone { get; protected set; }

        protected CommandInformation information;

        public Command(CommandInformation info)
        {
            information = info;
        }

        public abstract void Execute();
        public override string ToString() => $"[{this.GetType()}] {information}";
    }
}