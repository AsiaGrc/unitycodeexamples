﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Commands
{
    public class ChoiceDialogue : Command
    {
        public override bool isPreserved {get; protected set;} = false;
        ChoicePanel _choicePanel;

        public ChoiceDialogue(CommandInformation info) : base(info) { }

        public override void Execute()
        {
            _choicePanel = UIWidget.FindWidget<ChoicePanel>();
            _choicePanel.Show();
            string[] choicesText = new string[information.choices.Length];
            int[] choicesIndexes = new int[information.choices.Length];
            for(int i = 0; i < information.choices.Length; i++)
            {
                choicesText[i] = information.choices[i].text;
                choicesIndexes[i] = information.choices[i].index;
            }
            _choicePanel.Setup(choicesText, choicesIndexes, ChoiceMadeHandler);
        }

        public void ChoiceMadeHandler(int index)
        {
            InkManager.Instance.SelectChoice(index);
            _choicePanel.Hide();
            IsDone = true;
        }
    }
}