﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Commands
{
    public class CommandFactory
    {
        static CommandFactory _instance;

        public static CommandFactory Instance => _instance = _instance ?? new CommandFactory();



        private CommandFactory() { }



        public Command CreateCommand(CommandInformation information)
        {
            if(information.choices != null)
                return new ChoiceDialogue(information);

            if(information.text.ToLower().Contains(Const.Ink.endGame))
                return new EndGame(information);

            if(information.text.Contains(Const.Ink.phraseSeparator))
                return new PlayerDialogue(information);

            if(information.text.Contains(Const.Ink.command))
            {
                StringFormatter.Remove(information.text, Const.Ink.command);
                var commandName = information.GetCommandName();

                if(commandName == nameof(PlayMusic))
                    return new PlayMusic(information);
                else if(commandName == nameof(PlayAmbient))
                    return new PlayAmbient(information);
                else if(commandName == nameof(PlaySFX))
                    return new PlaySFX(information);
                else throw new NotSupportedException();
            }
            else throw new NotImplementedException();
        }


        public Command SaveChatCommand(CommandInformation information) => new SaveChatData(information);
    }
}