﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Commands
{
    public class CommandInvoker : MonoBehaviour
    {
        [SerializeField] TextAsset inkJsonAsset;


        InkManager _inkManager;
        CommandFactory _commandFactory;
        SaveLoadGameManager _saveLoadManager;
        RestoreState _chatStateMachine;


        void Awake()
        {
            _saveLoadManager = SaveLoadGameManager.Instance;
            _commandFactory = CommandFactory.Instance;
        }

        void OnEnable()
        {
            _chatStateMachine = new RestoreState();
            _chatStateMachine.RestoreChatState();
            _inkManager = new InkManager(inkJsonAsset, _chatStateMachine.State.current.state);
        }



        IEnumerator Start()
        {
            Debug.Log("Invoker started gameplay");
            while(true)
            {
                CommandInformation ci = _inkManager.GetNextCommandInformation();
                Command com = _commandFactory.CreateCommand(ci);
                Debug.Log($"<color=olive>{com}</color>");
                com.Execute();
                if(com.isPreserved)
                {
                    Command saveChatCom = _commandFactory.SaveChatCommand(ci);
                    saveChatCom.Execute();
                }
                yield return new WaitForCommand(com);
                _chatStateMachine.SaveInkCurrent(_inkManager.StoryState);
            }
        }


        void OnDisable()
        {
            _chatStateMachine.SaveChatState();
        }
    }



    public class WaitForCommand : CustomYieldInstruction
    {
        Command _com;

        public WaitForCommand(Command com)
        {
            _com = com;
        }

        public override bool keepWaiting => !_com.IsDone;
    }
}

