// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// NOTE: this shader is dedicated for use to draw water only
// thus, the mesh normals are suppose to be oriented UP
// this means that we don't use mesh normals here

Shader "YSI/Water Normals"
{
    Properties
    {
        _Color   ("Water Color", Color) = (1,1,1,1)
        _ColorCube ("Reflection Tint", Color) = (1,1,1,1)
        _ColorWave ("Wave Color", Color) = (1,1,1,1)
        _Normal1 ("Normal 1", 2D) = "black"  {}
        _Normal2 ("Normal 2", 2D) = "black"  {}
        [NoScaleOffset]
        _Cubemap ("Cubemap", Cube) = "black" {}
        _Contrib ("Normals x:N1, y:N2, z:Total, w:Shadow", Vector) = (1,1,1,1)
        _Speed   ("Speed xy:N1, zw:N2", Vector) = (1,1,1,1)
        _Wave    ("Wave White Tint x:low, y:high, z: contribution", Vector) = (0.7, 1, 1, 1)
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2  uv     : TEXCOORD0;
                fixed4 color  : COLOR;
            };

            struct v2f
            {
                float4 vertex  : SV_POSITION;
                float2 uv_n1    : TEXCOORD0;
                float2 uv_n2    : TEXCOORD1;
                float3 posWorld : TEXCOORD2;
                UNITY_FOG_COORDS(3)
                fixed4 color   : COLOR;
            };


            sampler2D _Normal1;
            sampler2D _Normal2;
            samplerCUBE _Cubemap;

            float4 _Normal1_ST;
            float4 _Normal2_ST;
            float4 _Speed;
            float4 _Contrib;
            fixed4 _Wave;
            fixed4 _Color;
            fixed4 _ColorCube;
            fixed4 _ColorWave;



            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);

                o.uv_n1 = v.uv * _Normal1_ST.xy + _Normal1_ST.zw + _Speed.xy * _Time.x * _Normal1_ST.xy;
                o.uv_n2 = v.uv * _Normal2_ST.xy + _Normal2_ST.zw + _Speed.zw * _Time.x * _Normal2_ST.xy;

                o.posWorld = mul(unity_ObjectToWorld, v.vertex).xyz;
                o.color = v.color;

                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }


            fixed4 frag (v2f i) : SV_Target
            {
                // sample normals
                float3 nmap1 = UnpackNormal(tex2D(_Normal1, i.uv_n1));
                float3 nmap2 = UnpackNormal(tex2D(_Normal2, i.uv_n2));
                // mix normals
                float3 wnorm = normalize(nmap1 * _Contrib.x + nmap2 * _Contrib.y);
                wnorm = lerp(float3(0,1,0), wnorm, _Contrib.z * 0.5);
                // obtain reflection
                float3 worldViewDir  = normalize(UnityWorldSpaceViewDir(i.posWorld));
                float3  worldRefl = reflect(-worldViewDir, wnorm);
                fixed4 texel_cube = texCUBE(_Cubemap, worldRefl);
                // get fake shadow
                fixed shadow = dot(float3(0,1,0), wnorm);
                shadow = saturate (shadow * shadow);
                // get fake wave foam
                fixed shadowStep = smoothstep(_Wave.x, _Wave.y, shadow) * _Wave.z;
                fixed4 waveColor = lerp((0).xxxx, _ColorWave, shadowStep);
                // apply waves color
                fixed4 res = _Color * lerp(1, shadow, _Contrib.w) + waveColor;
                // apply reflect
                res = res + texel_cube * _ColorCube;
                // apply fog
                UNITY_APPLY_FOG(i.fogCoord, res);
                return res;
            }
            ENDCG
        }
    }
}
