﻿Shader "YSI/Glitch"
{
    Properties
    {
        _MainTex ("Main Texture", 2D) = "white" {}
        _NoiseTex ("Noise Texture", 2D) = "white" {}
        _ColorIntensity ("Color Intensity", Range(0,1)) = 1
        _NoiseIntensity ("Noise Intensity", Range(0,1)) = 1
        _FlipIntensity("Flip Intensity", Range(0,1)) = 1
        _PeriodOne ("Period", Range(0, 1)) = 1
        _PeriodTwo ("Period", Range(0, 1)) = 1
    }

    SubShader
    {
        Tags { "Queue" = "Geometry" }

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"



            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float2 uv_Noise : TEXCOORD1;
                half4 period : TEXCOORD2;
                float4 vertex : SV_POSITION;
            };



            sampler2D _MainTex;
            float4 _MainTex_ST;

            sampler2D _NoiseTex;
            float4 _NoiseTex_ST;

            half _PeriodOne;
            half _PeriodTwo;
            half _ColorIntensity;
            half _NoiseIntensity;
            half _FlipIntensity;

            half _Timer;
            half _FlickerTime = 0.5;
            half _ColorRadius;

            half _NoiseScale;
            half _DisplacementFactor;

            half _FlipTime = 0.05;
            half _FlipUpTimer;
            half _FlipDownTimer;

            half _FlipUp;
            half _FlipDown;

            half2 _ColorDirection;


           half random (float2 uv)
           {
                return frac(sin(dot(uv,float2(12.9898,78.233)))*43758.5453123);
           }

           half PeriodicFunc()
           {
               half a = abs(dot(sin(_Time.y * _PeriodOne), cos(_Time.y * _PeriodTwo) - _CosTime));
               half b = abs(cos(_Time.y * _PeriodOne) + sin(_PeriodTwo) *sin(_Time.y * _PeriodTwo));
               half c = abs(sin(_Time.y*15));

               return saturate((a-b)*c);
           }

           half PeriodicFunc2()
           {
               half a = abs(dot(_SinTime.z - _CosTime.x, _CosTime.w *_SinTime.w));
               half c = abs(sin(_Time.y *50));

               return saturate(a*c);
           }

           void ColorChange(half4 periodicValue, float2 uv)
           {
               half period = periodicValue.x * _ColorIntensity;

               fixed  stepFactor = step(period, 0.5);

               half2 interCalc;
               interCalc.x = random(uv);
               interCalc.y = random(uv);

               half randValue = random(uv);

               _ColorDirection = lerp(0, interCalc, stepFactor);

               _ColorRadius = (randValue * 10 * _ColorIntensity, randValue, stepFactor);
           }

           void NoiseIntegration(half4 periodicValue, float2 uv)
           {
               half randValue = random(uv);

               fixed  stepFactor = step(randValue, periodicValue.x);
               _DisplacementFactor = lerp(periodicValue.x * _NoiseIntensity, 0, stepFactor);
               _NoiseScale = lerp(1 - randValue - _NoiseIntensity, 0, stepFactor);
           }

           void FlipUp(half4 periodicValue, float2 uv)
           {
                half randValue = random(uv);

                _FlipUpTimer = periodicValue.z;
                half stepFactor = step(_FlipUpTimer, 0.1);

                _FlipUp = lerp(randValue * _FlipIntensity, 0, stepFactor);
                _FlipUpTimer = lerp(0, _FlipUpTimer, stepFactor);
           }

           void FlipDown(half4 periodicValue, float2 uv)
           {
               half randValue = random(uv);

               _FlipDownTimer = periodicValue.y * 0.5;
               half stepFactor = step(_FlipDownTimer, 0.3);

               _FlipDown = lerp(1 - randValue * _FlipIntensity, 1, stepFactor);
               _FlipDownTimer = lerp(0, _FlipDownTimer, stepFactor);
           }




           v2f vert (appdata v)
           {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                o.uv_Noise = v.uv * _NoiseTex_ST.xy + _NoiseTex_ST.zw;
                o.period.x = PeriodicFunc();
                o.period.y = PeriodicFunc2();
                o.period.z = dot(PeriodicFunc(), PeriodicFunc2());
                o.period.w = 0;

                return o;
           }



           fixed4 frag (v2f i) : SV_Target
           {
                float2 uv = i.uv;

                FlipUp(i.period, i.uv);
                FlipDown(i.period, i.uv);

                //uv.y *= 2;
                //uv.y -= 1;
                //uv.y += _FlipUp;
                i.uv.y -= (1 - (i.uv.y + _FlipUp)) * step(i.uv.y, _FlipUp) + (1 - (i.uv.y - _FlipDown)) * step(_FlipDown, i.uv.y);

                NoiseIntegration(i.period, i.uv);

                half noise = tex2D(_NoiseTex, (i.uv_Noise - (0.5).xx) * _NoiseIntensity + float2(0.5, 0.5)).r;
                i.uv += (noise - 0.5) * _DisplacementFactor;

                ColorChange(i.period, i.uv);

                half3 col = tex2D(_MainTex, i.uv);
                half3 colR = tex2D(_MainTex, i.uv + _ColorDirection * _ColorRadius * 0.01);
                half3 colG = tex2D(_MainTex, i.uv - _ColorDirection * _ColorRadius * 0.01);

                half threshhold = 0.5;
                half lum = 0.5;

                col.rgb += colR.rbg * step(_ColorRadius, threshhold) * lum;
                col.rgb += colG.gbr * step(threshhold, _ColorRadius) * lum;

                return fixed4(col,1);
           }

           ENDCG
        }
    }
}
