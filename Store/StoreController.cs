﻿using UnityEngine;
using UnityEngine.Purchasing;
using System.Threading;
using System.Threading.Tasks;
using System;

namespace Controller
{
	public class StoreController : MonoBehaviour
	{
		[SerializeField] private UIStore _ui;

		StoreManager man;
		TaskCompletionSource<bool> _awaitStoreIsClosedTCS;

		public static BlinkType blinkType = BlinkType.Both;
		public enum BlinkType { Both, Coins, Cups }

		public Task TaskAwaitStoreIsClosed {
			get {
				Debug.Assert(_awaitStoreIsClosedTCS != null, "tcs is null");
				Debug.Assert(_awaitStoreIsClosedTCS.Task.IsCompleted == false, "tcs is completed");
				return _awaitStoreIsClosedTCS.Task;
			}
		}


		//.....................................................................................UNITY

		void Awake()
		{
			man = Manager.Store;
			_ui.SetOfferRegular(Manager.Profile.User.TimeOfferOfTheDayBought.Date == DateTime.Now.Date);
		}


		async void Start()
		{
			if (man.IsInitialized)
			{
				SetupUIPanel();
				return;
			}

			// for some reason store manager not inited
			await AwaitStoreInitializationAsync();

			if (man.IsInitialized)
			{
				SetupUIPanel();
			}
			else
			{
				if (man.WasInternetAvailable == false)
			 		await _ui.ShowNoInternetPopupAsync(CancellationToken.None);
				else
					await _ui.ShowInitializationFailed(CancellationToken.None);
				// gtfo
				ExitStore();
			}
		}


		void OnEnable()
		{
			man.OnSuccess += this.OnSuccess_Handler;
			man.OnFailed  += this.OnFailed_Handler;
			_awaitStoreIsClosedTCS = new TaskCompletionSource<bool>();
		}


		void OnDisable()
		{
			man.OnSuccess -= this.OnSuccess_Handler;
			man.OnFailed  -= this.OnFailed_Handler;
			_awaitStoreIsClosedTCS?.TrySetResult(true);
		}

		public void ShowSparks() => _ui.SetParticlesVisibility(true);

		//...................................................................................PRIVATE

		private async void SetupUIPanel()
		{
			Debug.Assert(man != null);
			Debug.Assert(man.IsInitialized);
			_ui.Setup( (man.ItemsProvider, man.StoreController.products.all) );
			_ui.OnBuyItemClicked += BuyProduct_Handler;
			_ui.OnCloseClicked += ExitStore;

			await _ui.ShowAsync(CancellationToken.None);
			if (blinkType == BlinkType.Both)
				_ui.Blink();
			else
			{
				_ui.BlinkOne(blinkType == BlinkType.Coins);
				blinkType = BlinkType.Both;
			}
		}


		private async Task AwaitStoreInitializationAsync()
		{
			_ui.ShowStandBy();
			if (man.IsInitializingNow == false)
				man.InitializeAsync();									// try re-initialize manager
			bool ok = await man.TaskInitialization;
			_ui.HideStandBy();
		}


		private async void ExitStore()
		{
			await _ui.HideAsync(CancellationToken.None);

			Manager.Scene.UnloadSpecial(ESpecialScenes.Store);
		}


		//............................................................................EVENT-HANDLERS

		void BuyProduct_Handler(SHash productHash)
		{
			Product product = man.StoreController.products.WithID(productHash.ToString());
			if (product != null && product.availableToPurchase)
			{
				_ui.ShowStandBy();
				man.StoreController.InitiatePurchase(product);
				Debug.Log("Store: Try purchasing " + productHash);
			}
			else
			{
				Debug.LogError("Cant purchase " + productHash);
				Debug.LogError($"Reason: {(product == null ? "is null" : "not available")}");
			}
		}


		public void OnSuccess_Handler(PurchaseEventArgs args)
		{
			_ui.HideStandBy();
			string productId = args.purchasedProduct.definition.id;
			ProductInfo product = man.ItemsProvider.GetProduct((SHash)productId);
			this.ApplyPurchaseAsync(product);
			_ui.ShowTransactionSuccessPopupAsync(product, CancellationToken.None);
			CLog.Info(string.Format("ProcessPurchase: PASS. Product: '{0}'", product.Hash), this);
			Debug.Log($"receipt: '{args.purchasedProduct.receipt}' | transaction id: '{args.purchasedProduct.transactionID}'");
			if (args.purchasedProduct.transactionID.NotEmpty())
				Manager.Analytics.StorePurchase(args.purchasedProduct);
		}


		public async void OnFailed_Handler(Product product, PurchaseFailureReason failureReason)
		{
			_ui.HideStandBy();
			switch (failureReason)
			{
				case PurchaseFailureReason.UserCancelled:
					//await _ui.ShowUserHasCancelledPopupAsync(CancellationToken.None);
					break;
				case PurchaseFailureReason.Unknown:
					_ui.ShowStandBy();
					bool internetAvailable = await Tools.CheckInternetReachable.TestAsync();
					_ui.HideStandBy();
					if (!internetAvailable)
					{
						await _ui.ShowNoInternetPopupAsync(CancellationToken.None);
						ExitStore();
						return;
					}
					else
						await _ui.ShowGenericError(CancellationToken.None, failureReason);
					break;
				default: await _ui.ShowGenericError(CancellationToken.None, failureReason); break;
			}
			Debug.LogError($"Store purchase FAIL. Product: '{ product.definition.storeSpecificId}', Reason: {failureReason}");
			// TODO: add analytic event
		}


		private async void ApplyPurchaseAsync(ProductInfo product)
		{
			if (product.Hash == (SHash)"starter_pack")
			{
				_ui.SetOfferRegular(true);
				Manager.Profile.User.TimeOfferOfTheDayBought = DateTime.Now;
			}

			var okCoin = await Manager.Profile.User.TryAddCoinsAsync(product.amount_coins);
			var okCup = await Manager.Profile.User.TryAddCupsAsync(product.amount_cups);

			if (product.amount_coins > 0) Manager.Audio.UI.Play("buy_coins");
			if (product.amount_cups > 0) Manager.Audio.UI.Play("buy_coffee");
		}
	}





	public struct ProductInfoView
	{
		public string title;
		public string description;
		public string realPrice; // price in $
		public string amount_coins;
		public string amount_cups;
		public string percent;

	
		public ProductInfoView(Product product, int coins, int cups, int prc)
		{
			title        = product.metadata.localizedTitle;
			description  = product.metadata.localizedDescription;
			realPrice    = product.metadata.localizedPriceString;
			amount_coins = coins.ToString();
			amount_cups  = cups .ToString();
			percent		 = prc  .ToString() + "%";
		}
	}
}