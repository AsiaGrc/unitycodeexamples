﻿using System;
using UnityEngine;

namespace Controller.Car
{
	[ExecuteInEditMode]
	[RequireComponent(typeof(Wheels))]

	public class BodyControl : MonoBehaviour
	{
		public Transform _body;

		private ControlPositions _controlPositions;
		public ControlPositions ContrPositions => _controlPositions;

		[SerializeField]
		private ControlOffsets _controlOffsets;
		public ControlOffsets ContrOffsets
		{
			get => _controlOffsets;
			set
			{
				_controlOffsets = value;
				//UpdateBody();
			}
		}

		private bool _inited = false;

		private Quaternion _initialRotation;
		private Vector3 _initialForward;
		private Vector3 _initialRight;

		private float _lowering = 0f;

		private void Start() => Init();

//#if UNITY_EDITOR
		void LateUpdate()
		{
			UpdateBody();
		}
//#endif

		private void UpdateBody()
		{
			if (!_inited) return;

			//x axis
			Vector3 front = ContrPositions.front;
			front.y += AdjustForLowering(ContrOffsets.front);

			Vector3 rear = ContrPositions.rear;
			rear.y += AdjustForLowering(ContrOffsets.rear);

			Quaternion rotX = Quaternion.FromToRotation(_initialForward, (front - rear).normalized) * _initialRotation;


			//z axis
			Vector3 right = ContrPositions.right;
			right.y += AdjustForLowering(ContrOffsets.right);

			Vector3 left = ContrPositions.left;
			left.y += AdjustForLowering(ContrOffsets.left);

			Quaternion rotZ = Quaternion.FromToRotation(_initialRight, (right - left).normalized) * _initialRotation;

			_body.localRotation = rotX * rotZ;

			//position
			Vector3 pos = Vector3.zero;
			pos.y = (ContrOffsets.front + ContrOffsets.rear) * .5f;
			pos.y += ContrOffsets.center;
			_body.localPosition = pos;
		}

		private float AdjustForLowering(float offset)
		{
			if (_lowering < 0 && offset < 0)
			{
				//return offset - _lowering; //or 0 if offset > lowering
				return offset * (1f + _lowering);
			}

			return offset;
		}

		public void SetLowering(float lowering)
		{
			_lowering = lowering;
		}

		private void OnDisable()
		{
			_body.localPosition = Vector3.zero;
			_body.localRotation = Quaternion.identity;
		}


		private void Init()
		{
			if (_inited) return;
			Wheels wd = GetComponent<Wheels>();

			var wheels = wd.WheelXs;

			_controlPositions.front = (wheels.fl.localPosition + wheels.fr.localPosition) * .5f;
			_controlPositions.rear = (wheels.rl.localPosition + wheels.rr.localPosition) * .5f;
			_controlPositions.left = (wheels.fl.localPosition + wheels.rl.localPosition) * .5f;
			_controlPositions.right = (wheels.fr.localPosition + wheels.rr.localPosition) * .5f;

			_controlPositions.center = (_controlPositions.front + _controlPositions.rear) * .5f;

			_controlOffsets = new ControlOffsets();

			_initialForward = Vector3.forward;
			_initialRight = Vector3.right;
			_initialRotation = Quaternion.identity;

			_inited = true;
		}

		[Serializable]
		public struct ControlPositions
		{
			public Vector3 front;
			public Vector3 rear;
			public Vector3 right;
			public Vector3 left;
			public Vector3 center;
		}

		[Serializable]
		public struct ControlOffsets
		{
			public float front;
			public float rear;
			public float right;
			public float left;
			public float center;
		}

	}
}