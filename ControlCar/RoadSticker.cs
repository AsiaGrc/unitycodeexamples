﻿using UnityEngine;
using UnityEngine.Assertions;
using System.Collections.Generic;
using System;


namespace Controller.Car
{
	[ExecuteInEditMode]
	[RequireComponent(typeof(Wheels))]

	public class RoadSticker : MonoBehaviour
	{
		private WheelData[] _wheels_;
		private WheelData[] Wheels { get { if (_wheels_ == null) _wheels_ = GetWheels(); return _wheels_; } }

		private List<int> _lHits = new List<int>();

		[SerializeField]
		private float _raycastOffset = 10f;
		[SerializeField]
		private float _maxWheelOffset = .15f;
		[SerializeField]
		private float _damping = 0f; //.1f for runtime (but its value should be set through animation) and 0f for editor.
		public float Damping { get => _damping; set => _damping = value; }

		private int _groundMask;

		private Vector3 _velNorm = Vector3.zero;
		private Vector3 _vel = Vector3.zero;
		private Vector3 _prevNorm;
		private Vector3 _prevPos;

		private Transform _target;

		private void Awake()
		{
			_target = transform;
		}

		public void ChangeTarget(Transform target)
		{
			_target = target;
		}

		//#if UNITY_EDITOR
		private void OnDisable()
		{
#if UNITY_EDITOR
			if (UnityEditor.BuildPipeline.isBuildingPlayer) return;		// hotfix: this throws after a while
#endif
			ResetWheels();
		}

		private void LateUpdate()
		{
			AlignCar();
			AlignWheels();
		}
//#endif


		private WheelData[] GetWheels()
		{
			IWheelSpinner[] wheelSpinners = GetComponent<Wheels>().AllWheelSpinners.all;
			WheelData[] wheels = new WheelData[wheelSpinners.Length];

			for (int i = 0; i < wheelSpinners.Length; i++)
			{
				wheels[i].wheel = wheelSpinners[i].Transform;
				wheels[i].child = wheels[i].wheel.GetChild(0);
				wheels[i].radius = wheelSpinners[i].WheelRadius;
			}

			_groundMask = LayerMask.GetMask("Ground");
			Assert.IsFalse(_groundMask == 0, "Layer 'Ground' does not Exist");

			_prevNorm = transform.up;
			_prevPos = transform.position;

			if (wheels.Length > 4)
				Debug.LogWarning("Ensure this script works fine for (click)", this);

			return wheels;
		}

		private void CalculateGroundPoints()
		{
			Assert.IsNotNull(Wheels, "wheels array is null");
			for (int i = 0; i < Wheels.Length; i++)
				Wheels[i].didHit = Physics.Raycast(Wheels[i].wheel.position + transform.up * _raycastOffset, -transform.up, out Wheels[i].hit, _groundMask);
		}

		private void AlignCar()
		{
			CalculateGroundPoints();

			Vector3 mediumPos = Vector3.zero;

			for (int i = 0; i < Wheels.Length; i++)
			{
				if (Wheels[i].didHit)
				{
					_lHits.Add(i);
					mediumPos += Wheels[i].hit.point;
				}
			}

			int triangles = _lHits.Count - 2;
			//check if all wheels are on the ground
			//One triangle would turn the car upside down if the front wheels are out of the collider
			//because we cannot tell in which order are they on the collider making the normal point in the opposite
			if (triangles <= 1) { _lHits.Clear(); return; }

			Vector3[] triangleNormals = new Vector3[triangles];
			Vector3 mediumNorm = Vector3.zero;
			for (int i = 0; i < triangles; i++)
			{
				if (i % 2 == 0)
					triangleNormals[i] = Vector3.Cross(Wheels[_lHits[i + 1]].hit.point - Wheels[_lHits[i]].hit.point, Wheels[_lHits[i + 2]].hit.point - Wheels[_lHits[i]].hit.point).normalized;
				else
					triangleNormals[i] = Vector3.Cross(Wheels[_lHits[i + 1]].hit.point - Wheels[_lHits[i]].hit.point, Wheels[_lHits[i + 1]].hit.point - Wheels[_lHits[i + 2]].hit.point).normalized;
				mediumNorm += triangleNormals[i];
			}

			for (int i = 0; i < triangleNormals.Length; i++)
				Debug.DrawLine(transform.position, transform.position + triangleNormals[i], Color.green);

			mediumNorm /= triangles;
			mediumPos /= _lHits.Count;

			//interpolate between previous and the norm
			mediumNorm = Vector3.SmoothDamp(_prevNorm, mediumNorm.normalized, ref _velNorm, _damping);
			_prevNorm = mediumNorm;

			Debug.DrawLine(transform.position, transform.position + mediumNorm * 2f, Color.yellow);

			mediumPos.x = transform.position.x;
			mediumPos.z = transform.position.z;

			mediumPos = Vector3.SmoothDamp(_prevPos, mediumPos, ref _vel, _damping);
			_prevPos = mediumPos;

			_target.rotation = Quaternion.FromToRotation(transform.up, mediumNorm) * transform.rotation;
			_target.position = mediumPos;

			_lHits.Clear();
		}

		private void AlignWheels()
		{
			CalculateGroundPoints();

			for (int i = 0; i < Wheels.Length; i++)
			{
				if (Wheels[i].didHit)
				{
					Transform wheel = Wheels[i].child;
					Vector3 pos = wheel.localPosition;

					pos.y = Wheels[i].radius * transform.localScale.y - (Wheels[i].wheel.position - Wheels[i].hit.point).magnitude;
					if (Mathf.Abs(pos.y) > _maxWheelOffset) pos.y = Mathf.Sign(pos.y) * _maxWheelOffset;

					wheel.localPosition = pos / transform.localScale.y;
				}
			}
		}

		private void ResetWheels()
		{
			for (int i = 0; i < Wheels.Length; i++)
			{
				Vector3 pos = Wheels[i].child.localPosition;
				pos.y = 0;

				Wheels[i].child.localPosition = pos;
			}

			_prevNorm = transform.up;
			_prevPos = transform.position;
		}

#if UNITY_EDITOR
		private void OnDrawGizmosSelected()
		{
			if (!enabled) return;

			for (int i = 0; i < Wheels.Length; i++)
			{
				Color prev = UnityEditor.Handles.color;

				if (Wheels[i].didHit)
				{
					Debug.DrawLine(Wheels[i].wheel.position, Wheels[i].hit.point, new Color(1f, 1f, 1f, .3f));
					Debug.DrawLine(Wheels[i].hit.point, Wheels[i].hit.point + Wheels[i].hit.normal * .2f, new Color(0f, 1f, 0f));
				}
			}
		}
#endif

		[Serializable]
		private struct WheelData
		{
			public Transform wheel;
			public Transform child;
			public float radius;
			public RaycastHit hit;
			public bool didHit;
		}

	}
}